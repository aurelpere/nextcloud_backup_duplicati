# nextcloud_backup

Pipeline ci/cd ansible pour automatiser le backup d'un serveur nextcloud avec duplicati/ovh s3 object storage

La pipeline est programmée pour tourner tous les jours à 6h sur le serveur nextcloud.alternatiba47.com (voir depot nextcloud_prod)

Les résultats sont visibles dans le menu à gauche CI/CD


# prerequisites on server

`python3`<br>

# Gitlab secret à renseigner:

`LOGIN`: server login<br>
`SUDO_PASSWORD`:  server sudo password<br>
`OVH_S3_PASS` : Ovh s3 secret key<br>
`SERVER_SSH_PORT` :  ssh port<br>
`SSH_PRIVATE_KEY`: ovh object storage region<br>


# variables d'environnement à définir dans ansible

Les variables dans group_vars/all/variables.yml sont également à renseigner

